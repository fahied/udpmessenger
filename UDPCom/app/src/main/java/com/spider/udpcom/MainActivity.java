package com.spider.udpcom;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.example.androidpreferencefragment.R;

import java.io.IOException;
import java.net.SocketException;

import utils.UDPMessenger;

public class MainActivity extends Activity {

    // Preference keys
    private static final String KEY_DESTINATION_IP_ADDRESS = "destinationIP";
    private static final String KEY_DESTINATION_PORT_ADDRESS = "destinationPort";

    //UI
    CheckBox broadCastCheckBox;

    // private var
    private String destinationIP = "127.0.0.0";
    private int destinationPort = 20002;
    private Boolean broadcasting;


    // UDP messenger
    private UDPMessenger udpMessenger;
    private SharedPreferences sharedPreferences;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //UI
        setContentView(R.layout.activity_main);
        broadCastCheckBox = (CheckBox)findViewById(R.id.isHostCheckBox);
        //intialize
        udpMessenger = new UDPMessenger();
        try {
            udpMessenger.startUDPMessenger();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //load preference into local var
    	loadPref();
    }

    /** Called when the user touches the button */
    public void sendMessageSequence(View view) throws IOException, InterruptedException {
        // Do something in response to button click
        Button button = (Button)view;
        String messageSequence = sharedPrefValueForKey(button.getText().toString());
        handleUDPRequest(messageSequence);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		/*
		 * Because it's only ONE option in the menu.
		 * In order to make it simple, We always start SetPreferenceActivity
		 * without checking.
		 */
		
		Intent intent = new Intent();
        intent.setClass(MainActivity.this, SetPreferenceActivity.class);
        startActivityForResult(intent, 0);
        return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*
		 * To make it simple, always re-load Preference setting.
		 */
		
		loadPref();
	}
    
	private void loadPref()
    {
		// broad to all preference
		boolean checkbox_preference = sharedPreferences.getBoolean("checkbox_preference", false);
		broadCastCheckBox.setChecked(checkbox_preference);
        broadcasting = checkbox_preference;


        //Destination IP preference
        destinationIP   = sharedPreferences.getString(KEY_DESTINATION_IP_ADDRESS,"127.0.0.1");
        String port = sharedPreferences.getString(KEY_DESTINATION_PORT_ADDRESS, "20002");
        destinationPort = Integer.parseInt(port);

	}

    private String sharedPrefValueForKey(String key)
    {
        return sharedPreferences.getString(key,"");
    }


    private void handleUDPRequest(String sequenceMessage) throws InterruptedException, IOException {
        String[]parts = sequenceMessage.split("\n");
        for (int i = 0; i < parts.length;i++)
        {
            String part = parts[i];
            //if the message part is numeric then probably it is delay otherwise a message to be sent
            if (isNumeric(part))
            {
                int delay  = Integer.parseInt(parts[i]);
                Thread.sleep(delay);
            }
            else if (!part.equalsIgnoreCase(""))
            {
                if (broadcasting)
                    udpMessenger.broadcastUDPMessage(part,destinationPort);
                else udpMessenger.sendUDPMessage(part,destinationPort,destinationIP);
            }
        }
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            udpMessenger.stopUDPMessenger();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}
