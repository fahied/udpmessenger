package com.spider.udpcom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import utils.IPAddressValidator;

public class SetPreferenceActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {

    SharedPreferences prefs;
    // Preference keys
    private static final String KEY_DESTINATION_IP_ADDRESS = "destination_ip";
    private static final String KEY_DESTINATION_PORT_ADDRESS = "destination_port";
    private IPAddressValidator ipAddressValidator;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PrefsFragment()).commit();
        ipAddressValidator = new IPAddressValidator();
	}

    @Override
    protected void onResume() {
        super.onResume();
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        SharedPreferences.Editor prefEditor = prefs.edit();
        if(s.equals(KEY_DESTINATION_IP_ADDRESS)){
            String optionValue = prefs.getString(KEY_DESTINATION_IP_ADDRESS, "");
            if(!ipAddressValidator.validateIP(optionValue)){
                showErrorDialog("Wrong IP","I dont like the option");
                prefEditor.putString(KEY_DESTINATION_IP_ADDRESS, "127.0.0.1");
                prefEditor.commit();
                //reload();
            }
        }
        else if(s.equals(KEY_DESTINATION_PORT_ADDRESS)){
            String optionValue = prefs.getString(KEY_DESTINATION_PORT_ADDRESS, "");
            if(!ipAddressValidator.validatePort(optionValue)){
                showErrorDialog("Wrong Port","I dont like the option");
                prefEditor.putString(KEY_DESTINATION_PORT_ADDRESS, "10002");
                prefEditor.commit();
            }
        }
        return;
    }


    private void showErrorDialog(String dialogTitle,String errorString){
        String okButtonString = "OK";
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle(dialogTitle);
        ad.setMessage(errorString);
        ad.setPositiveButton(okButtonString,new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                reload();
            } } );
        ad.show();
        return;
    }

    private void reload(){
        startActivity(getIntent());
        finish();
    }

}
