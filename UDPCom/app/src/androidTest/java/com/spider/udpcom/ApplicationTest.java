package com.spider.udpcom;

import android.app.Application;
import android.test.ApplicationTestCase;

import utils.IPAddressValidator;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    public void validIPTest() {
        final String correctIP = "127.3.2.1";
        final String  wrongIP = "1234.3.3.1";
        IPAddressValidator ipAddressValidator = new IPAddressValidator();
        assertEquals(ipAddressValidator.validateIP(correctIP), true);
        assertEquals(ipAddressValidator.validateIP(wrongIP), false);
    }


}